import { Component } from '@angular/core';
import { PanelComponent } from './components/panel/panel.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'memory-game';
}
