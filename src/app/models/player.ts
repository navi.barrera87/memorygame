export interface Player{
	name: string;
	score: number;
	victories: number;
	turn: boolean;
	cards?: any;
}