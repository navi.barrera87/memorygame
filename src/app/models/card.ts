export interface Card{
	number: number;
	turned: boolean;
	taken: boolean;
}