import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Card } from '../../models/card';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.sass']
})
export class CardComponent implements OnInit {
  @Output() flipEvent = new EventEmitter<Card>();
  @Input() cantFlip: boolean;
  @Input() small: boolean = false;
  env = environment;
  items = {
  	0 : 'apple',
  	1 : 'banana',
  	2 : 'coconut',
  	3 : 'grape',
  	4 : 'lemon',
  	5 : 'orange',
  	6 : 'pear',
  	7 : 'pineapple',
  	8 : 'watermelon'
  };
  @Input() card: Card = <Card>{};

  constructor() { }

  ngOnInit() { }

  flip() {
  	if(this.card.taken||this.card.turned||this.cantFlip)
  		return;
  	this.card.turned = true;
  	this.flipEvent.emit(this.card);
  }

  flipDemo(){
    this.card.turned = true;
    setTimeout(() => {
      this.card.turned = false;
    },1000);
  }

}
