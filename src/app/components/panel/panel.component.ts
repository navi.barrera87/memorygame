import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators, NgForm, ValidationErrors } from '@angular/forms';
import { Player } from '../../models/player';
import { Card } from '../../models/card';
import { environment } from '../../../environments/environment';
import { PlayerComponent } from '../player/player.component';
import { BoardComponent } from '../board/board.component';
import { CardComponent } from '../card/card.component';

@Component({
  selector: 'panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.sass']
})
export class PanelComponent implements OnInit {
  env = environment;
  @ViewChild('formDirective',{static: false}) private formDirective: NgForm;
  @ViewChild('animatedBoard',{static: false}) boardChild: BoardComponent;
  playerForm: FormGroup;
  players: Array<Player> = [];
  cards: Array<Card> = [];
  cardsBackground: Array<Card> = [];
  numberPlayers = 2;
  rows = 3;
  columns = 6;
  fruits = 9;
  fruitsArray = [];
  turn: boolean;
  turnAnimation: boolean = false;
  playing: boolean = false;
  over: boolean = false;
  small: boolean = true;
  interval: any;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.playerForm = this.formBuilder.group({
      players: this.formBuilder.array([])
    });
    const array = <FormArray>this.playerForm.controls['players'];
    for(let i = 0; i < this.numberPlayers; i++){
      this.players.push({name: '', score: 0, victories: 0, turn: false, cards:[]});
      array.push(this.initPlayerRow());
    }

    let picks = this.pickRandom(17, 17, Array.from(Array(144).keys()));
    this.cardsBackground = this.shuffle([...picks, ...picks]).map(number => {
      return {number:number%9, turned:false, taken:false};
    });
  }

  ngAfterViewInit() {
    this.interval = setInterval(()=>{this.boardChild.flipDemo()}, 2000);
  }

  initPlayerRow(): FormGroup{
    return this.formBuilder.group({
      name: ['',[Validators.required]]
    });
  }

  submitPlayers() {
    if(!this.validate())
      return;
    this.startGame();
    clearInterval(this.interval);
  }

  startGame() {
    this.playing = true;
    this.over = false;
    this.players.map(player => {
      player.turn = false;
      player.score = 0;
      player.cards = []
    });
    this.turn = Math.random() < 0.5;
    this.players[Number(this.turn)].turn = true;

    this.fruitsArray = Array.from(Array(this.fruits).keys());
    let picks = this.pickRandom(this.rows, this.columns, this.fruitsArray);
    this.cards = this.shuffle([...picks, ...picks]).map(number => {
      return {number:number, turned:false, taken:false};
    });
  }

  endGame() {
    if(this.players[0].score==this.players[1].score)
      console.log('tie!');
    if(this.players[0].score>this.players[1].score){
      console.log('player: '+ this.players[0].name + 'wins!');
      this.players[0].victories++;
    }
    if(this.players[0].score<this.players[1].score){
      console.log('player: '+ this.players[1].name + 'wins!');
      this.players[1].victories++;
    }
    this.playing = false;
    this.over = true;
  }

  pickRandom (rows, columns, array) {
    const items = (rows * columns) / 2;
    const clonedArray = [...array];
    const randomPicks = [];

    for (let index = 0; index < items; index++) {
        const randomIndex = Math.floor(Math.random() * clonedArray.length);
        
        randomPicks.push(clonedArray[randomIndex]);
        clonedArray.splice(randomIndex, 1);
    }

    return randomPicks;
  }

  shuffle(array) {
    const shuffledArray = [...array];

    for (let index = shuffledArray.length - 1; index > 0; index--) {
        const randomIndex = Math.floor(Math.random() * (index + 1));
        const original = shuffledArray[index];

        shuffledArray[index] = shuffledArray[randomIndex];
        shuffledArray[randomIndex] = original;
    }

    return shuffledArray;
  }

  updateScore($event){

    this.players[Number(this.turn)].score += Number($event.matched);
    this.players[Number(this.turn)].turn = $event.matched;
    if($event.matched){
      let card = JSON.parse(JSON.stringify($event.card));
      card.taken = false;
      card.turned = true;
      this.players[Number(this.turn)].cards.push(card);
    }

    if(this.players.reduce( ( sum, { score } ) => sum + score , 0) == this.fruits){
      this.endGame();
      return;
    }

    if(!$event.matched)
      this.turnAnimation = true;
    setTimeout(() => {
      this.turn = ($event.matched) ? this.turn: !this.turn;
      this.players[Number(this.turn)].turn = true;
      this.turnAnimation = false;
    },2000);
  }

  validate(){
    let valid = true;
    for(let schema of this.playerForm.controls['players']['controls']){
      Object.keys(schema.controls).forEach( key => {
        const controlErrors: ValidationErrors = schema.get(key).errors;
        if(controlErrors)
          valid = false;
      });
    }
    return valid;
  }

  toNumber(bool) {
    return Number(bool);
  }

}
