import { Component, OnInit, Input } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Player } from '../../models/player';

@Component({
  selector: 'player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.sass'],
  animations: [
  	trigger(
      'upAnimation', 
      [
        transition(
          ':enter', 
          [
            style({ marginTop: '10px',opacity: 0}),
            animate('0.6s ease-out', 
                    style({ marginTop: '0px',opacity: 1}))
          ]
        )
      ]
    )
  ]
})
export class PlayerComponent implements OnInit {
  
  @Input() player: Player = <Player>{};
  small: boolean = true;

  constructor() { }

  ngOnInit() { }

}
