import { Component, OnInit, Input, Output, EventEmitter, ViewChildren, QueryList } from '@angular/core';
import { Card } from '../../models/card';
import { CardComponent } from '../card/card.component';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.sass']
})
export class BoardComponent implements OnInit {
  env = environment;
  @Input() cards;
  @Input() small: boolean;
  @Output() scoreEvent = new EventEmitter();
  @ViewChildren(CardComponent) cardViewChildren: QueryList<CardComponent>;
  selectedCards: Array<Card> = [];
  cantFlip: boolean = false;

  constructor() {}

  ngOnInit() {}

  ngAfterViewInit() {}

  checkPair($event) {
  	if(this.selectedCards.length < 2)
  	  this.selectedCards.push($event);

  	if(this.selectedCards.length == 2){
      this.cantFlip = true;
  	  let matched = this.selectedCards[0].number == this.selectedCards[1].number;
  	  setTimeout(()=> {
  	  	this.selectedCards[0].turned = matched;
  	  	this.selectedCards[0].taken  = matched;
  	  	this.selectedCards[1].turned = matched;
  	  	this.selectedCards[1].taken  = matched;
  	  	this.selectedCards = [];
        this.cantFlip = false;
  	  },1000);
  	  this.scoreEvent.emit({matched:matched,card:this.selectedCards[0]});
  	}
  }

  flipDemo(){
    let demoCards = this.cardViewChildren.toArray();
    demoCards[Math.floor(Math.random() * demoCards.length)].flipDemo();
    demoCards[Math.floor(Math.random() * demoCards.length)].flipDemo();
  }

}
